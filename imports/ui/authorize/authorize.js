/* global $ */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
// localStorage
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

import { oauthClients } from '../../api/collection.js';
import { pageSession } from '../../api/client/reactive.js';

import '../login/login.js';
import './authorize.html';

Template.authorize.onCreated(function () {
  pageSession.set('loading', true);
  const data = FlowRouter.current().queryParams;
  const handleAuth = this.subscribe('authorizedOAuth');
  const handleClient = this.subscribe('oauthClient', data.client_id);
  if (handleAuth.ready() && handleClient.ready()) {
    pageSession.set('loading', false);
  }
  pageSession.set('viewlog', 'login');
});

// Get the login token to pass to oauth
// This is the best way to identify the logged user
Template.authorize.helpers({
  getToken() {
    return localStorage.getItem('Meteor.loginToken');
  },
  getClient() {
    return oauthClients.findOne();
  },
  viewlog() {
    return pageSession.get('viewlog');
  },
  isAuthorizedClients(clientId) {
    const user = Meteor.user();
    if (user && user.oauth && user.oauth.authorizedClients && user.oauth.authorizedClients.includes(clientId)) {
      return true;
    }
    return false;
  },
});

Template.authorize.events({
  'click #logout-oauth'(event) {
    event.preventDefault();
    return Meteor.logout();
  },
  'click #cancel-oauth'(event) {
    event.preventDefault();
    return window.close();
  },
});

// Auto click the submit/accept button if user already
// accepted this client
Template.authorize.onRendered(function () {
  this.autorun((c) => {
    const user = Meteor.user();
    if (user && user.oauth && user.oauth.authorizedClients && user.oauth.authorizedClients.includes(this.data.client_id)) {
      c.stop();
      Meteor.setTimeout(() => {
        $('#form').submit();
      }, 2000);
    }
  });
});
