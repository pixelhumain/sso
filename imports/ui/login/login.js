import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
// import { TAPi18n } from 'meteor/tap:i18n';
// import { IonPopup } from 'meteor/meteoric:ionic';
import { DDP } from 'meteor/ddp-client';

// helpers
import { IsValidEmail } from 'meteor/froatsnook:valid-email';

import { pageSession } from '../../api/client/reactive.js';

import './login.html';

// methods
// getcitiesbypostalcode, getcitiesbylatlng, createUserAccountRest

const pushFrontUrl = Meteor.settings.public.remoteUrl;
const PushRemote = DDP.connect(pushFrontUrl);

Template.login.onCreated(function () {
  pageSession.set('error', false);
  pageSession.set('loading-logging', false);
});

Template.login.onRendered(function () {
  pageSession.set('error', false);
});

Template.login.events({
  'submit .login-form'(event) {
    event.preventDefault();
    const email = event.target.email.value;
    const password = event.target.password.value;
    if (!email || !password) {
      pageSession.set('error', 'Not completed all fields');
      return;
    }

    if (!IsValidEmail(email)) {
      pageSession.set('error', 'Email not valid');
      return;
    }
    pageSession.set('loading-logging', true);
    Meteor.loginAsPixel(email, password, (error) => {
      if (!error) {
        pageSession.set('loading-logging', false);
        pageSession.set('error', null);
      } else {
        pageSession.set('loading-logging', false);
        pageSession.set('error', error.reason);
        return null;
      }
    });
  },
  'click .signin-view-js'(event) {
    event.preventDefault();
    pageSession.set('viewlog', 'signin');
  },
});
Template.login.helpers({
  loadingLogging() {
    return pageSession.get('loading-logging');
  },
  error() {
    return pageSession.get('error');
  },
  viewlog() {
    return pageSession.get('viewlog');
  },
});

Template.login.onCreated(function () {
  pageSession.set('error', false);
  pageSession.set('loading-signup', false);
  pageSession.set('cities', null);
});

Template.signin.onRendered(function () {
  pageSession.set('error', false);
  pageSession.set('cities', null);
  pageSession.set('codepostal', null);
  pageSession.set('cityselect', null);
});

Template.signin.events({
  'keyup #codepostal, change #codepostal'(event) {
    if (event.currentTarget.value.length === 5) {
      PushRemote.call('getcitiesbypostalcode', event.currentTarget.value, function (error, data) {
        pageSession.set('cities', data);
      });
    } else {
      pageSession.set('cities', null);
    }
  },
  'submit .signup-form'(event) {
    event.preventDefault();
    pageSession.set('error', null);
    const trimInput = (val) => val.replace(/^\s*|\s*$/g, '');
    let city;
    const email = trimInput(event.target.email.value);
    const username = trimInput(event.target.username.value);
    const password = event.target.password.value;
    const repassword = event.target.repassword.value;
    const name = trimInput(event.target.name.value);
    const codepostal = trimInput(event.target.codepostal.value);
    if (event.target.city && event.target.city.value) {
      city = event.target.city.value;
    }

    if (!email || !password || !repassword || !name || !codepostal || !city || !username) {
      pageSession.set('error', 'Not completed all fields');
      return;
    }

    const isValidCodepostal = (val) => {
      if (val.length === 5) {
        return true;
      }
      pageSession.set('error', 'Postcode must be 5 digits');
      return false;
    };

    const isValidName = (val) => {
      if (val.length >= 6) {
        return true;
      }
      pageSession.set('error', 'Name is Too short');
      return false;
    };

    const isValidUsername = (val) => {
      if (val.length >= 6) {
        return true;
      }
      pageSession.set('error', 'Username is Too short');
      return false;
    };

    const isValidPassword = (val) => {
      if (val.length > 7) {
        return true;
      }
      pageSession.set('error', 'Password is Too short');
      return false;
    };

    if (!isValidName(name)) {
      return;
    }
    if (!isValidUsername(username)) {
      return;
    }
    if (!IsValidEmail(email)) {
      pageSession.set('error', 'Email not valid');
      return;
    }
    if (!isValidPassword(password)) {
      return;
    }
    if (!isValidCodepostal(codepostal)) {
      return;
    }
    if (password !== repassword) {
      pageSession.set('error', 'Not the same password');
      return;
    }

    // verifier
    const user = {};
    user.email = email;
    user.password = password;
    user.name = name;
    user.username = username;
    user.repassword = repassword;
    user.codepostal = codepostal;
    // numero insee
    user.city = city;

    pageSession.set('loading-signup', true);
    pageSession.set('error', null);
    // createUserAccount or createUserAccountRest
    // console.log(user);
    PushRemote.call('createUserAccountRest', user, (error) => {
      if (error) {
        pageSession.set('loading-signup', false);
        // console.log(error);
        pageSession.set('error', error.error);
      } else {
        pageSession.set('loading-signup', false);
        pageSession.set('error', null);
        pageSession.set('viewlog', 'login');
        // Todo - message de confirmation que l'inscription c'est bien passé et qu'i faut validé son email
        // Todo - message d'erreur "Email not unique" pas visible depuis inscription sso
        // Todo - redirection aprés l'inscription vers login
        /* Meteor.loginAsPixel(email, password, (err) => {
          if (!err) {
            pageSession.set('loading-signup', false);
            pageSession.set('error', null);
            // return Router.go('/');
          } else {
            pageSession.set('loading-signup', false);
            if (err.reason === 'notValidatedEmail') {
              // eslint-disable-next-line no-underscore-dangle
              // IonPopup.alert({ template: TAPi18n.__(err.reason) });
              // return Router.go('/login');
            }
            pageSession.set('error', err.reason);

            return false;
          }
        }); */
      }
    });
  },
  'click .login-view-js'(event) {
    event.preventDefault();
    pageSession.set('viewlog', 'login');
  },
});

Template.signin.helpers({
  loadingLogging() {
    return pageSession.get('loading⁻signup');
  },
  error() {
    return pageSession.get('error');
  },
  city() {
    return pageSession.get('cities');
  },
  citySelected() {
    if (pageSession.get('cityselect') === this.insee) {
      return 'selected';
    }
    return undefined;
  },
  codepostal() {
    return pageSession.get('codepostal');
  },
  viewlog() {
    return pageSession.get('viewlog');
  },
});
