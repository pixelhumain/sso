import { Meteor } from 'meteor/meteor';
import { oauthClients } from '../../api/collection.js';

Meteor.startup(function () {
  // ajout communecter config client
  if (Meteor.settings.sso && Meteor.settings.sso.name && !oauthClients.findOne({ title: Meteor.settings.sso.name })) {
    // const CLIENT_ID = '4JZxkcThcKT5FHJnk';
    // const CLIENT_SECRET = 'qkdWS8jJjVzQR5gHUFRikX10SHxVjiOv3vZGAZkSnvl';
    // const REDIRECT_URI = 'http://communecter.org/connect/co/oauth';
    // const AUTH_URI = 'http://localhost:3000/oauth/authorize';
    // const TOKEN_URI = 'http://localhost:3000/oauth/token';
    // grant_type=authorization_code
    // title, homepage, description, privacyLink, redirectUris, grants, clientId, secret
    oauthClients.insert({
      title: Meteor.settings.sso.name,
      redirectUris: [Meteor.settings.sso.redirectUri],
      grants: ['authorization_code', 'refresh_token'],
      clientId: Meteor.settings.sso.clientId,
      secret: Meteor.settings.sso.clientSecret,
    });
  }
  if (oauthClients.find({ name: { $exists: true } }).count() > 0) {
    oauthClients.find({ name: { $exists: true } }).forEach(function (client) {
      if (!client.title) {
        oauthClients.insert({
          title: client.name, redirectUris: [client.redirectUri], grants: ['authorization_code', 'refresh_token'], clientId: client.clientId, secret: client.clientSecret,
        });
        oauthClients.remove({ _id: client._id });
      }
    });
  }
});
