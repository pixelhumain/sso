import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

FlowRouter.route('/', {
  whileWaiting() {
    this.render('loading');
  },
  action() {
    this.render('layout', 'home', {
      headerTitle: 'headerTitleHome',
    });
  },
});

FlowRouter.route('/oauth/authorize', {
  name: 'authorize',
  whileWaiting() {
    this.render('loading');
  },
  action(params, qs) {
    this.render('layout', 'authorize', {
      headerTitle: 'headerTitleAuthorize',
      client_id: qs.client_id,
      redirect_uri: qs.redirect_uri,
      response_type: qs.response_type,
      state: qs.state,
    });
  },
});

FlowRouter.route('/oauth/error/:error', {
  name: 'oauth404',
  whileWaiting() {
    this.render('loading');
  },
  action(params, qs) {
    this.render('layout', 'oauth404', {
      headerTitle: 'headerTitleOauth404',
      error: qs.error,
    });
  },
});
