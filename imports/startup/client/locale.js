/* global _ */
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { moment } from 'meteor/momentjs:moment';

const trad = {
  en: {
    Authorise: 'Authorise',
    Logout: 'Logout',
    Cancel: 'Cancel',
    loading: 'loading...',
    You_are_logged_in_as: 'You are logged in as',
    will_be_able_to: 'will be allowed to connect to',
    is_allowed_to_connect: 'is authorized to connect to communecter',
    email: 'Email',
    password: 'Password',
    username: 'Username',
    loggin: 'Sign in',
    'create an account': 'Create an account',
    name: 'Name',
    repassword: 'Re-password',
    postcode: 'Postcode',
    signin: 'Sign up',
    'already have an account': 'Already have an account',
    'do not have an account': 'Do not have an account',
    Welcome: 'Welcome',
    'Communecter Login Failed': 'Communecter Login Failed',
    'Email not valid': 'Email not valid',
    'Email not unique': 'Email not unique',
    'Not completed all fields': 'Not completed all fields',
    'Postcode must be 5 digits': 'Postcode must be 5 digits',
    'Name is Too short': 'Name is Too short',
    'Password is Too short': 'Password is Too short',
    'Not the same password': 'Not the same password',
    'are you sure you want to delete': 'Are you sure you want to delete ?',
    'Error not postal code match': 'Error not postal code match',
    Reset: 'Reset',
    Send: 'Send',
    accountPending: "You've been invited : please resume the registration process in order to log in.",
    betaTestNotOpen: 'Our developpers are fighting to open soon ! Check your mail that will happen soon !',
    notValidatedEmail: 'Your account is not validated : please check your mailbox to validate your email address.',
    emailNotFound: 'Impossible to find an account for this username or password.',
    emailAndPassNotMatch: 'Email or password does not march. Please try again !',
    city: 'City',
  },
  fr: {
    Authorise: 'Autoriser',
    Logout: 'Se déconnecter',
    Cancel: 'Annuler',
    You_are_logged_in_as: 'Vous êtes connecté en tant que',
    will_be_able_to: 'sera autorisé à se connecter à communecter',
    is_allowed_to_connect: 'est autorisé à se connecter à communecter, vous allez être redirigé',
    email: 'Email',
    username: "Nom d'utilisateur",
    password: 'Mot de passe',
    loggin: "S'identifier",
    'create an account': 'Créer un compte',
    name: 'Nom',
    repassword: 'Vérification mot de passe',
    postcode: 'Code postal',
    signin: "S'inscrire",
    'already have an account': 'Vous avez déja un compte',
    'do not have an account': "Vous n'avez pas encore un compte",
    Welcome: 'Bonjour',
    'Communecter Login Failed': 'Échec de la connexion',
    'Email not valid': 'Email non valide',
    'Email not unique': 'Email déja présent',
    'Not completed all fields': 'Vous devez renseigner tous les champs',
    'Postcode must be 5 digits': 'le code postal doit être de 5 chiffres',
    'Name is Too short': 'Votre nom est trop court',
    'Password is Too short': 'Votre mot de passe est trop court',
    'Not the same password': "Vous n'avez pas saisis le même mot de passe",
    'are you sure you want to delete': 'Etes-vous sûr que vous voulez supprimer ?',
    'Error not postal code match': 'Erreur code postal non trouvé',
    notValidatedEmail: "Votre compte n'est pas validé : un mail vous a été envoyé sur votre boite mail.",
    emailNotFound: "impossible de trouver un compte avec ce nom d'utilisateur ou cet email.",
    emailAndPassNotMatch: 'Email ou Mot de Passe ne correspondent pas, rééssayez.',
    city: 'Ville',
  },
};

const languageBrowser = () => {
  const localeFromBrowser = window.navigator.userLanguage || window.navigator.language;
  let locale = 'en';

  if (localeFromBrowser.match(/en/)) locale = 'en';
  if (localeFromBrowser.match(/fr/)) locale = 'fr';

  return locale;
};
const Helpers = {};

let languageText = {};

// eslint-disable-next-line no-underscore-dangle
const _languageDeps = (Meteor.isClient) ? new Tracker.Dependency() : null;
let currentLanguage = 'en';

// language = 'en'
Helpers.setLanguage = function (language) {
  if (language && language !== currentLanguage) {
    currentLanguage = language;
    if (Meteor.isClient) _languageDeps.changed();
  }
};

Helpers.language = function () {
  if (Meteor.isClient) _languageDeps.depend();
  return currentLanguage;
};

Helpers.setDictionary = function (dict) {
  languageText = dict;
};

Helpers.addDictionary = function (dict) {
  _.extend(languageText, dict);
};

// handleCase will mimic text Case making src same case as text
const handleCase = function (text, src) {
  // Return lowercase
  if (text === text.toLowerCase()) return src.toLowerCase();
  // Return uppercase
  if (text === text.toUpperCase()) return src.toUpperCase();
  // Return uppercase first letter, rest lowercase
  if (text.substr(1) === text.substr(1).toLowerCase()) return src.substr(0, 1).toUpperCase() + src.substr(1).toLowerCase();
  // Return src withour changes
  if (text.substr(0, 2) === text.substr(0, 2).toUpperCase()) return src;
  // Return CamelCase
  return src.replace(/( [a-z])/g, function ($1) {
    return $1.toUpperCase();
  });
};

Helpers.getText = function (text, lang) {
  const txt = text.toLowerCase();
  const langText = languageText && languageText[txt];
  const langKey = (typeof lang === 'string') ? lang : Helpers.language();
  return handleCase(text, (langText) ? ((langText[langKey]) ? langText[langKey] : langText.en) : `[${text}]`);
};

Meteor.startup(() => {
  Tracker.autorun(() => {
    let language;

    language = languageBrowser();

    // console.log(language);
    moment.locale(language);
    Helpers.setLanguage(language);
  });
});

// template helpers
Template.registerHelper('langChoix', () => Helpers.language());

Template.registerHelper('_', (word) => {
  const language = trad[Helpers.language()] ? Helpers.language() : 'en';
  if (trad[language] && trad[language][word]) {
    return trad[Helpers.language()][word];
  }
  return word;
});
