/* global _ $ */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

Meteor.startup(function () {
  window.HTML.isConstructedObject = function (x) {
    return _.isObject(x) && !$.isPlainObject(x);
  };
});

Template.registerHelper('equals', function (a, b) {
  return (a === b); // Only text, numbers, boolean - not array & objects
});

Template.registerHelper('urlImageCommunecter', function () {
  return Meteor.settings.public.urlimage;
});
