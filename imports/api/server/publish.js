import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { oauthClients } from '../collection.js';

Meteor.publish('oauthClient', function (clientId) {
  check(clientId, String);
  if (!this.userId) {
    return this.ready();
  }
  return oauthClients.find({
    clientId,
  }, {
    fields: {
      title: 1,
    },
  });
});
