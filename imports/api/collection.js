import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const oauthClients = new Mongo.Collection('oauth_clients');
